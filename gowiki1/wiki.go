package main

import "fmt"
import "io/ioutil"
import "net/http"

type Page struct {
    Title string 
    Body []byte // []byte means 'a 'byte' slice' more info -> http://blog.golang.org/go-slices-usage-and-internals
}

// this is a method named save that takes as its receiver p, a pointer to Page . It takes no parameters, and returns a value of type error." 
func (p *Page) save() error {
    filename := p.Title + ".txt"
    return ioutil.WriteFile(filename, p.Body, 0600)
}

/*  The function loadPage constructs the file name from the title parameter, reads the file's contents into a new variable body, and returns a pointer to a Page literal constructed with the proper title and body values.

Functions can return multiple values. The standard library function io.ReadFile returns []byte and error. In loadPage, error isn't being handled yet; the "blank identifier" represented by the underscore (_) symbol is used to throw away the error return value (in essence, assigning the value to nothing).

But what happens if ReadFile encounters an error? For example, the file might not exist. We should not ignore such errors. Let's modify the function to return *Page and error. */
func loadPage(title string) (*Page, error) {
    filename := title + ".txt"
    body, err := ioutil.ReadFile(filename)
    if err != nil {
        return nil, err
    }
    return &Page{Title: title, Body: body}, nil
}
/* View Handler - Will allow users to view a page. It will handle URLs prefixed with "/view/". */
func viewHandler(w http.ResponseWriter, r *http.Request) {
    title := r.URL.Path[len("/view/"):]
    p, _ := loadPage(title)
    fmt.Fprintf(w, "<h1>%s</h1?<div>%s</div>", p.Title, p.Body)
}

/* Main Function */
func main() {
/*    p1 := &Page{Title: "TestPage", Body: []byte("This is a sample Page.")}
    p1.save()
    p2, _ := loadPage("TestPage")
    fmt.Println(string(p2.Body)) */
    http.HandleFunc("/view/", viewHandler)
    http.ListenAndServe(":8080", nil)
}


